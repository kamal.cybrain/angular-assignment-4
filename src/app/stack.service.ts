import { Injectable } from '@angular/core';
import { StackService } from './stack.service';

@Injectable({
  providedIn: 'root'
})
export class Stack {
   stack: any[]=[];
   push(item: any) {
    this.stack.push(item);
  }
  pop() {
    return this.stack.pop();
  }

  constructor(private stackService: StackService) { }
  myStack = new StackService.Stack();
  


  mystack(){
    this.myStack.push('item 1');
    this.myStack.push('item 2');
    this.myStack.push('item 3');
    
    // remove items from the stack
    console.log(this.myStack.pop()); // output: item 3
    console.log(this.myStack.pop()); // output: item 2
    console.log(this.myStack.pop()); // output: item 1

  }

}
export { StackService };

