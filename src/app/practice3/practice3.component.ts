import { Component } from '@angular/core';
//ques 2:
interface nnnnn {
  Id: number;
  FirstName: string;
  LastName: string;
  Email: string;
  company:string;
  contact:number ;
}
//ques3:
interface IData {
Id: number;
FirstName: string;
LastName: string;
Email: string;
ContactNo: string;
}

@Component({
  selector: 'app-practice3',
  templateUrl: './practice3.component.html',
  styleUrls: ['./practice3.component.scss']
})
export class Practice3Component {
  constructor(){
    //ques1 
    this.obarray();
    //ques2
    this.appdata();
    //ques20
    this.dataset();
    //ques3
    this.addData();
    //ques4:
    console.log(this.kk);

    //ques8,10,13,14,15,20,16:
    this.onit();
    this.c();
    this.pp();
    this.list1();
    this.twodarray();
    console.log( this.removedElements);
    console.log(this.copiedArray);
    this.fruit();
    this.stack1();

  }
  //ques1:
  myArray : string[] = ['a','b','c'];
  


  obarray(){
   

      this.myArray = this.myArray.concat('d'); 

    

  }

   //ques2:
   DataSet1: nnnnn[] = [
    { Id: 1, FirstName: "John", LastName: "Doe", Email: "john.doe@example.com" ,company:"xyz" ,contact:1234},
    { Id: 2, FirstName: "Jane", LastName: "Doe", Email: "jane.doe@example.com" ,company:"abc",contact:2222 },
    
  ];

  
  DataSet: { Id: number, FirstName: string, LastName: string, Email: string, company:string,contact:number }[] = [
    { Id: 1, FirstName: "kamal", LastName: "swain", Email: "kamal.cybrain@gmail.com", company:"cybrain", contact: 12346789 },
    { Id: 2, FirstName: "sajid", LastName: "ali", Email: "sajid.digit@gmail.com", company:"digit", contact:1222222 },
    
  ];
   
  //ques3:
  public DataSet2: IData[] = [
    {
      Id: 1,
      FirstName: "kamal",
      LastName: "swain",
      Email: "kamal@22gmail.com",
      ContactNo: "123456790"
    },
    {
      Id: 2,
      FirstName: "sajid",
      LastName: "ali",
      Email: "sajidali@gmail.com",
      ContactNo: "2345678919"
    },
    {
      Id: 3,
      FirstName: "prince",
      LastName: "ray",
      Email: "princeray.cybrain@gmail.com",
      ContactNo: "3456789312"
    }
  ];

  addData() {
    const newData: IData = {
      Id: 4,
      FirstName: "aditya",
      LastName: "raj",
      Email: "adityaraj@23devilgmail.com",
      ContactNo: "56789364412"
    };
    this.DataSet2.push(newData);
  }

  //ques4:
  kk=this.DataSet.push();

  //5.Can the push() function be used to add elements to the beginning of an array?
//sol:When you want to add an element to the end of your array, use push() . If you need to add an element to the beginning of your array, use unshift() . If you want to add an element to a particular location of your array, use splice().
  
//ques6:
newData= {
  Id: 3, FirstName: "kamal", LastName: "swain", Email: "kamal.swain@gmail.com", company:"cybrain", contact: 12346789 }
  appdata(){
    
    if (!this.DataSet.some(data => data.Email === this.newData.Email)) {
      this.DataSet.push(this.newData);
      
    }
  }

   //ques7:
   array1 :string[]= ['kamal'];
   array2 :string[]= ['nayan'];
   addd:string[]=[...this.array1, ...this.array2];

   //ques8:
pp(){
  const pop=this.addd.pop();

}

//9 - What is the return value of the pop() function?
//sol:it will delete all the value at the end.


//ques10:
myArray1 = [];
onit(){
  if (this.myArray1.pop() === undefined) {
    console.log("The array is empty.");
  } else {
    console.log("The array is not empty.");
  }

}

//11.Can the pop() function be used to remove elements from the beginning of an array?
//sol:no,If you want to remove an item from an array, you can use the pop() method to remove the last element or the shift() method to remove the first element.

//ques12:
stack1(){

  class Stack {
        private items: any[] = [];
      
        push(item: any): void {
          this.items.push(item);
        }
      
        pop(): any {
          return this.items.pop();
        }
      
        isEmpty(): boolean {
          return this.items.length === 0;
        }
      
      }
      
      let myStack = new Stack();
      myStack.push(1);
      myStack.push(2);
      myStack.push(3);
      
      console.log(myStack.pop()); // Output: 3
      console.log(myStack.pop()); // Output: 2
      console.log(myStack.pop()); // Output: 1
      console.log(myStack.isEmpty()); // Output: true
      
    }


//ques13:
public common = ["apple", "banana", "cherry", "date", "elderberry","grapes","orange"];

c(){
  this.common.splice(2, 1);

}

//ques14:
list=[1,2,3,4,5,12,23,44]
list1(){

  this.list.splice(2, 2);
}

//ques15:
originalArray = [1, 2, 3, 4, 5,6,7,8,9];
removedElements = this.originalArray.splice(1, 2);

//ques16:
fruits:string[] = ['apple', 'banana', 'cherry','watermelon','orange','papaya'];
fruit(){
  
  this.fruits.splice(1, 1, 'kiwi');
  
  console.log(this.fruits);
}

//17.What is the difference between the slice() and splice() functions?
//sol:The slice() method can be used to create a copy of an array or return a portion of an array. It is important to note that the slice() method does not alter the original array but instead creates a shallow copy. Unlike the slice() method, the splice() method will change the contents of the original array.

//ques18:
originalArray1 = [1, 2, 3, 4, 5,6,7,8,9];

copiedArray = this.originalArray1.slice();


    //ques19:
    dataset(){
      this.DataSet.sort((a, b) => b.Id - a.Id);
      console.log(this.DataSet);
      

  }


  //ques20:
 data = [
  [1, "papaya"],
  [3, "grapes"],
  [2, "watermelon"],
  [4, "day"]
];


twodarray(){
  const idArray = this.data.map(subArray => subArray[0]);
}

}
  
  



